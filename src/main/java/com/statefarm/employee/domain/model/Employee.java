package com.statefarm.employee.domain.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
public class Employee   implements Serializable {

    private static final long serialVersionUID = 1L;

    
    @Id
    int  Id;
    
    @Column(name = "FIRST_NAME", nullable = true, length = 25)
    private String firstName;

    @Column(name = "MIDDLE_NAME", nullable = true, length = 25)
    private String middleName; 
    
    @Column(name = "LAST_NAME", nullable = true, length = 25)
    private String lastName;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
    
    
}
