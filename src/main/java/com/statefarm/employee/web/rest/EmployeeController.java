package com.statefarm.employee.web.rest;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.statefarm.employee.domain.model.Employee;
import com.statefarm.employee.exception.ResourceNotFoundException;
import com.statefarm.employee.service.IEmployeeService;

 

@RestController
public class EmployeeController {

    @Autowired
    private IEmployeeService  employeeService;


    /*
    Basic Welcome method
     */
    @GetMapping("/welcome")
    public String welcome(){
        return "Welcome to Spring Boot";
    }


    /*
    To get All the employee details as a list from DataBase
     */
    @GetMapping("/getEmployees")
    public ResponseEntity<List<Employee>> getAllEmployees(){
       List<Employee> employees = employeeService.getAllEmployeeDetails();
        if(employees == null) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(employees, HttpStatus.OK);
        //return new employee ("1", "vamsi","", "nallani");
    }


    
   

    /*
     * Get employee details based on employee id
     * To get a single the data through single id/name or any data
     * Using - Path Variable => used when we know the data for which we need to display
     * PostMan => /employee/55
     */
    @GetMapping("/getEmployee/{employeeId}")
    public  Employee getEmployeeById(@PathVariable int employeeId) throws ResourceNotFoundException{
    	return employeeService.getEmployeeById(employeeId);
    	 
    }
     
   /*
    To save/create the employee details to the DataBase
     */
    @PostMapping("/createEmployeeDetails")
    public ResponseEntity<Employee> createEmployeeDetails(@RequestBody Employee employee){

    	Employee employeeDetails=employeeService.saveDetails(employee);
        if(employeeDetails == null) {
 			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
 		}
 		return new ResponseEntity<>(employeeDetails, HttpStatus.OK);
    }


    /*
    To update the employee details to the Database
     */

    @PutMapping("/updateEmployeeDetails")
    public  Employee updateEmployeeDetails(@RequestParam  int employeeId,@RequestBody Employee employee) throws ResourceNotFoundException{
    	return employeeService.updateDetails(employeeId, employee);
        
    }

    
    /*
    To delete the employee details from the Database
     */
    
    @DeleteMapping("/deleteEmployeeDetails")
    public Map<String, Boolean> deleteEmployeeDetails(@RequestParam int employeeId) throws ResourceNotFoundException{
        return employeeService.deleteDetails(employeeId);
    }



}




