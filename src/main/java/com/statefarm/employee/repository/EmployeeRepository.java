package com.statefarm.employee.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.statefarm.employee.domain.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
	  
	  
	  List<Employee> findByFirstName(String firstName);
	  void deleteById(int employeeId);
}