package com.statefarm.employee.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.statefarm.employee.domain.model.Employee;
import com.statefarm.employee.exception.ResourceNotFoundException;
import com.statefarm.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public List<Employee> getAllEmployeeDetails(){
      return employeeRepository.findAll() ;
    }

  public  Employee getEmployeeById(int employeeId) throws ResourceNotFoundException{

    	Employee emp= employeeRepository.findById(employeeId).orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
    	  return emp;
    }
    
    
    
    
    public Employee saveDetails(Employee employee){
        return employeeRepository.save(employee);
    }


    public Employee updateDetails(int employeeId, Employee employeeDeatils) throws ResourceNotFoundException{
         Employee employee=employeeRepository.findById(employeeId).orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
      // employee.setId(employeeDeatils.getId());
       employee.setFirstName(employeeDeatils.getFirstName());
       employee.setMiddleName(employeeDeatils.getMiddleName());
       employee.setLastName(employeeDeatils.getLastName());
       final Employee updatedEmployee=employeeRepository.save(employee);
         return   updatedEmployee;
    }

    public Map<String,Boolean> deleteDetails(int employeeId) throws ResourceNotFoundException{
    	  Employee employee=employeeRepository.findById(employeeId).orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
          employeeRepository.delete(employee);
          Map<String,Boolean> response=new HashMap<String, Boolean>();
          response.put("deleted", Boolean.TRUE);
          return response;
    }

}
