package com.statefarm.employee.service;

import java.util.List;
import java.util.Map;

import com.statefarm.employee.domain.model.Employee;
import com.statefarm.employee.exception.ResourceNotFoundException;
 
public interface IEmployeeService {
	    public List<Employee> getAllEmployeeDetails();
	    public Employee getEmployeeById(int employeeId) throws ResourceNotFoundException ;
	    public Employee saveDetails(Employee employee);
	    public Employee updateDetails(int employeeId, Employee employee) throws ResourceNotFoundException ;
	    public Map<String, Boolean> deleteDetails(int employeeId) throws ResourceNotFoundException ;
}
