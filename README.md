# Assignment 1 - EmployeeCrudOperation

This assignment will provide hands-on knowledge and experience in building a REST API using Spring Boot. This will also provide knowledge how to interact with Database. This will make one familiar with Gitlab and Git Bash.

# Getting Started

Create a Spring Boot application which produces a REST API that will have the following features:
a.	Create an Employee (POST)
b.	Update an Employee (Patch/PUT)
c.	Get one/list of Employee(s) (GET)
d.	Delete an Employee (DELETE)

Make sure the REST API payload is in JSON format and use Spring REST library to achieve the above.  Use maven to build your project.

Use in memory database H2 for storing the data for Employee. While Starting the application the database should be populated with employees.

The Employee table should have columns like FirstName, MiddleName, LastName and EmployeeID (auto generated and primary key).

# Prerequisites

Git Bash: Git Bash is an application for Microsoft Windows environments which provides an emulation layer for a Git command line experience.

STS: Spring enabled Java Development IDE.

Java: JDK 1.8 for building and running Java programs.

Maven: Maven for building Java programs.

Postman:  Testing Rest APIs

H2 Database: In memory database for persisting the data.

# To run

To run the application, you can pull this code from here: https://gitlab.com/vamsi_n/employeecrudoperation.git.
This sample application is available on port 8080.

# Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method in the EmployeeApplication class from your IDE.

# Running the tests

Once started, you can navigate to http://localhost:8080/swagger-ui.html#/. This tells you that the server is up and ready to test the scenarios. 
This lets you inspect the API using an intuitive UI

